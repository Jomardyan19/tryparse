﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp93
{
    class Program
    {
        static void Main(string[] args)
        {
            Test("100");
            Test(null);
            Test("");
            Test("329f");
            Test("524");
            Test("-54");
            Test("-5");
            Test(int.MaxValue.ToString());
            Test(int.MinValue.ToString());
            Console.ReadLine();
        }
        static void Test(string value)
        {
            if (TryParse(value, out int n))
            {
                Console.WriteLine("Value is  " + value);
                Console.WriteLine("Number is " + n);
            }
            else
            {
                Console.WriteLine(value + " is not Number ");
                Console.WriteLine("default number is " + n);
            }
            Console.WriteLine("________________");
        }

        static bool TryParse(string str, out int n)
        {
            if (string.IsNullOrEmpty(str))
            {
                n = default(int);
                return false;
            }
            bool isMinus = str[0] == '-';
            int startIndex = isMinus ? 1 : 0;

            int num = 0;
            for (int i = startIndex; i < str.Length; i++)
            {
                if (char.IsNumber(str[i]))
                    num = num * 10 + Parse(str[i]);
                else
                {
                    n = default(int);
                    return false;
                }
            }

            n = isMinus ? -num : num;
            return true;
        }
        static int Parse(char dig)
        {
            switch (dig)
            {
                case '1': return 1;
                case '2': return 2;
                case '3': return 3;
                case '4': return 4;
                case '5': return 5;
                case '6': return 6;
                case '7': return 7;
                case '8': return 8;
                case '9': return 9;
                default: return 0;
            }
        }

    }
}
